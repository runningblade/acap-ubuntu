#!/bin/bash
if [ ! -d "../ACAP-build" ]; then
  mkdir ../ACAP-build
fi
cd ../ACAP-build

if [ ! -d "ACAP-build" ]; then
  mkdir ACAP-build
fi
cd ACAP-build

cmake ../../ACAP -DMATLAB_ROOT=/nas/longleaf/apps/matlab/2016a
make
