#!/bin/bash
if [ ! -d "../ACAP-build" ]; then
  mkdir ../ACAP-build
fi
cd ../ACAP-build

if [ ! -d "CoMISo-build" ]; then
  mkdir CoMISo-build
fi
cd CoMISo-build

export GMM_DIR=../../ACAP/gmm/include
cmake ../../ACAP/CoMISo
make
